import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Data } from './data';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb(){
    const info = [
      { id: 1, name: 'Alif', location: 'Dhaka' ,gender: 'Male',password: '123'},
      { id: 2, name: 'juggernaut', location: 'Safe Lane' ,gender: 'Male',password: '123'},
      { id: 3, name: 'invoker', location: 'Mid Lane' ,gender: 'Male',password: '123'},
      { id: 4, name: 'Ember Spirit', location: 'Mid Lane',gender: 'Male', password: '321'}
    ];
    return{info};
  }
  genId(info:Data[]) : number{
    return info.length > 0 ? Math.max(...info.map(info=>info.id))+1:1;
  }

}
