import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Data } from '../data';
import { DataService } from '../data.service';
//import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  // loading =false;
  submitted = false;
  // returnUrl: string;
@Input() user:Data;
@Input() check:boolean;
@Input() valid = false;
info: Data[];
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ){}

    // private authenticationService : AuthenticationService
  // ) {
  //   if(this.authenticationService.currentUserValue){
  //     this.router.navigate(['/']);


  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
    name: [''],
    password: ['']
    })
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.getinfo();
  }
  // get f(){
  //    return this.loginForm.controls;
  // }
  getinfo():void {
    this.dataService.getInfo()
    .subscribe(info => this.info = info);
  }

  onSubmit(){
    this.submitted=true;
    var tval;
    for(var val of this.info){
      if(val.name.toUpperCase( )==this.loginForm.value.name.toUpperCase( ) && val.password.toUpperCase( )===this.loginForm.value.password.toUpperCase( )){
        tval=val;
        this.valid=true;
      }
      // test comment
    }
    if(this.valid===true){
      console.log("validation check  "+this.valid);
      this.check=true;
    }
    else{
      this.check=true;
      console.log("validation check  "+this.valid);
     // console.log("validation check  "+!this.valid);
    }
    // this.loading=true;
    // this.authenticationService.login(this.f.name.value, this.f.password.value).subscribe();
  }

}
