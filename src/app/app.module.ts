import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule }    from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
// import { AdminLoginComponent } from './admin-login/admin-login.component';
import { HttpClientModule} from '@angular/common/http';
import { HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import { InMemoryDataService} from './in-memory-data.service';
// import { UserDetailComponent } from './user-detail/user-detail.component';
import { FormsModule } from '@angular/forms';
// import { UserSearchComponent } from './user-search/user-search.component';
import { HomeComponent } from './home/home.component';
import { TermsComponent } from './terms/terms.component';
import { ChartsModule } from 'ng2-charts';
import { ChartComponent } from './chart/chart.component';
import { AmChartComponent } from './am-chart/am-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    // SignupComponent,
    LoginComponent,
    // AdminLoginComponent,
    // UserDetailComponent,
    // UserSearchComponent,
    HomeComponent,
    TermsComponent,
    ChartComponent,
    AmChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService,{dataEncapsulation:false}),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
