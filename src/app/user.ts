export class User {

    constructor(
        public id: number,
        public name: string,
        public gender: string,
        public location: string,
        public password: string
    ){}
}
