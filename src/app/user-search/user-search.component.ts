import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Data } from '../data';
import { DataService } from '../data.service';
import {debounceTime,distinctUntilChanged,switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent implements OnInit {

  info$: Observable<Data[]>;
  private searchTerms = new Subject<string>();

  constructor( private dataService: DataService) { }

  search(term: string): void{
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.info$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term:string) => this.dataService.searchUser(term)),
    );
  }

}
