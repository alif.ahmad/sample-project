import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Data } from '../data';
import { DataService } from '../data.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  registerForm: FormGroup;
  checked = false;
  submitted = false;
  gender =[];
  locations: string[]=['Dhaka', 'Chittagong','Sylhet','Barisal','Rajshahi'];

  constructor(private dataService: DataService,
              private formBuilder: FormBuilder,
              private router: Router){}
              

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      location: [''],
      gender: [''],
      password: ['', Validators.required],
      checkbox: ['', Validators.required]
    });
    this.gender = this.getGender();
  }

  getGender(){
    return [
      { id: '1', name: 'Male'},
      { id: '2', name: 'Female'},
      { id: '3', name: 'Other'}
    ];
  }


  onSubmit(){
    this.submitted = true;
    this.dataService.register(this.registerForm.value ).subscribe();
    this.router.navigate(['/login']);
  }
  // add(name: string, location:string, gender:string,password:string):void{
  //   name = name.trim();
  //   location = location.trim();
  //   gender = gender.trim();
  //   password = password.trim();
  //   if(!name && !location && !gender &&!password){return ;}
  //   this.dataService.addUser({name,location,gender,password} as Data)
  //       .subscribe(user => {this.info.push(user);});
  // }

  
  @Input() user:Data;

  

}

