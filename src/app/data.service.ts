import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Data } from './data';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http : HttpClient) {    
  }
  httpOptions = { headers: new HttpHeaders({'Content-type': 'application/json'})};
  private url = 'api/info';

  getInfo(): Observable<Data[]> {
    return this.http.get<Data[]>(this.url);
 }
//  addUser( {name,location,gender,password}: User): Observable<User>{
//    return this.http.post<Data>(this.url, {name: name, location:location,gender:gender,password:password}, this.httpOptions);
//  }
 deleteUser(user: Data|number): Observable<Data>{
   const id = typeof user==='number'? user:user.id;
   const url = `${this.url}/${id}`
   return this.http.delete<Data>(url,this.httpOptions);
 }
//  addAll(user: Data){
//    return this.http.post<Data>(this.url, user,this.httpOptions);
//  }
 updateUser(user: Data):Observable<any>{
   return this.http.put(this.url, user, this.httpOptions);
 }
 register(user: Data){
   return this.http.post(this.url,user,this.httpOptions);
 }
 getUser(id:number):Observable<Data>{
   const Userurl = `${this.url}/${id}`;
   return this.http.get<Data>(Userurl);
 }
 searchUser(term: string):Observable<Data[]>{
   if(!term.trim()){
     return of ([]);
   }
   return this.http.get<Data[]>(`${this.url}/?name=${term}`);
 }

}
