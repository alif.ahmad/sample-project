import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { SignupComponent } from './signup/signup.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { TermsComponent } from './terms/terms.component';
import { ChartComponent } from './chart/chart.component';
import { AmChartComponent } from './am-chart/am-chart.component';


const routes: Routes = [
  // grouping routes for lazy loading
  { path: "users",
  // children: [
  //   { path: '', component: AdminLoginComponent },
  //   { path: "signup", component: SignupComponent},
  //   { path: "adminlogin", component: AdminLoginComponent},
  //   { path: "detail/:id", component: UserDetailComponent}
  // ]
  loadChildren: ()=> import('./user-detail/user-detail.module').then(m=> m.UserDetailModule)
},
  { path: "login", component: LoginComponent},
  { path: "terms", component: TermsComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full'},
{path: 'charts', component: ChartComponent},
{path: 'am-charts', component: AmChartComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
