import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLoginComponent } from '../admin-login/admin-login.component';
import { SignupComponent } from '../signup/signup.component';
import { UserDetailComponent } from './user-detail.component';


const routes: Routes = [
  { path: '', component: AdminLoginComponent },
  { path: "signup", component: SignupComponent},
  { path: "adminlogin", component: AdminLoginComponent},
  { path: "detail/:id", component: UserDetailComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserDetailRoutingModule { }
