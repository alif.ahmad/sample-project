import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailComponent } from './user-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InMemoryDataService } from '../in-memory-data.service';
import { SignupComponent } from '../signup/signup.component';
import { UserSearchComponent } from '../user-search/user-search.component';
import { AdminLoginComponent } from '../admin-login/admin-login.component';
import { UserDetailRoutingModule } from './user-detail-routing.module';



@NgModule({
  declarations: [UserDetailComponent,
                 SignupComponent,
                 UserSearchComponent,
                 AdminLoginComponent],
  imports: [
    CommonModule,
    UserDetailRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService,{dataEncapsulation:false}),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserDetailModule { }
