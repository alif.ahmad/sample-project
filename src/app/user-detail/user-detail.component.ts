import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Data } from '../data';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  updateForm: FormGroup;
  submitted = false;

@Input() user: Data;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.updateForm = this.formBuilder.group({
      id: [''],
      name: [''],
      location: [''],
      gender: [''],
      password: ['']
    })
    this.getUser();
  }

  onSubmit(): void{
    this.submitted = true;
    console.log(this.updateForm.value);
    this.dataService.updateUser(this.updateForm.value).subscribe();
    // redirecting using router
    this.router.navigate(['/users/adminlogin']);
  }

  getUser(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getUser(id)
        .subscribe(user => this.user = user);
  }



}
