export interface Data{
    id: number;
    name: string;
    location?: string;
    gender?: string;
    password: string;
}