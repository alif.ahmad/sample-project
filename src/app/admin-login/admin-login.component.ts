import { Component, OnInit, Input } from '@angular/core';
import { Data } from '../data';
import { DataService } from '../data.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  constructor(private dataService: DataService) { }

  info: Data[];

@Input() user: Data;

  ngOnInit(): void {
    this.getinfo();
  }
  getinfo(): void {
    this.dataService.getInfo()
    .subscribe(info => this.info = info);
  }
  // add(name:string): void{
  //   name = name.trim();
  //   if(!name){return;}
  //   this.dataService.addUser({ name } as Data).subscribe(user => {this.info.push(user);
  // });}
  delete(user: Data): void{
    this.info = this.info.filter(h => h !== user);
    this.dataService.deleteUser(user).subscribe();
  }
  save(): void{
    this.dataService.updateUser(this.user).subscribe();
  }

}
